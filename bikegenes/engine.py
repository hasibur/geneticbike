
import sys
import pylab
from bike import *
from terrain import *
from math import sin, cos
from acel import *

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
#from matplotlib.collections import PatchCollection
import matplotlib.path as mpath
import matplotlib.patches as mpatches
#import matplotlib.lines as mlines


g = -9.8
k = 1
# terrain parameter
N_POINTS =  5000

class Derivative:
    """Class representing velocity and acceleration."""
    def __init__(self, dx, dy, dvx, dvy):
        self.dx, self.dy, self.dvx, self.dvy = dx, dy, dvx, dvy


def force_spring(circ1, circ2, k12, l12):
    """The function calculates the force betweem two circles.
     
    Force acting on 1!
    """
    x1=circ1.x
    x2=circ2.x
    y1=circ1.x
    y2=circ2.y

    dis= math.sqrt( (x2-x1)**2 + (y2-y1)**2 )
    F12_x= -k12 * (  (x2-x1) - l12* (x2-x1)/dis )
    F12_y= -k12 * (  (y2-y1) - l12* (y2-y1)/dis )
    return F12_x, F12_y

def force_bike(bike, i):
    """
    """
    fx, fy = 0, 0
    for j in range(1, bike.NID):
        if j == i:
            continue
        c1 = bike.circles(i)
        c2 = bike.circles(j)
        k = bike.springs(i,j).k
        l = bike.springs(i,j).l0
        fx, fy =+ force_spring(c1, c2, k, l)
    return fx, fy


def touch(circle, terrain):
    """Touch function"""
    
    mean = 0
    cont = 0
    for i in xrange(terrain.npoints):
        x = terrain.x(i)
        y = terrain.y(x)
        if circle.cdist(x, y) <= circle.radius:
            mean += i
            cont += 1
    if cont == 0:
        return -1
    else:
        return int(mean*1.0/cont)

def revert(circle, alpha, dv):
    """Mirror function"""
    
    vx = circle.vx
    vy = circle.vy
    #rotate vector
    vxi = vx * cos(alpha) + vy * sin(alpha)
    vyi = - vx * sin(alpha) + vy * cos(alpha)
    #reflect ortogonal velocity
    vyi = -vyi
    #add rotation if needed
    if circle.flag == 'ON':
        vxi += dv
    #rotate again
    circle.vx = vxi * cos(alpha) - vyi * sin(alpha)
    circle.vy = vxi * sin(alpha) + vyi * cos(alpha)
    return circle
    

class Particle:
    def __init__(self, bike):
         self.bike = bike
         self.st1 = bike.circle1
         self.st2 = bike.circle2
         self.st3 = bike.circle3
         self.st4 = bike.circle4
         
    def acceleration(self, state, t):
        """Calculate acceleration caused by other objects! on this one.
        """        
        #fx , fy = - k * state.x, g - k * state.y
        ax, ay = a_bike(self.bike, state.ID)
        return (ax, ay)

    def initialDerivative(self, state, t):
        """Part of Runge-Kutta method."""
        ax, ay = self.acceleration(state, t)
        return Derivative(state.vx, state.vy, ax, ay)

    def nextDerivative(self, initialState, derivative, t, dt):
        """Part of Runge-Kutta method."""
        state =  Circle(initialState.ID, initialState.mass, initialState.radius,
                         0.0, 0.0, initialState.flag)
        state.x = initialState.x + derivative.dx*dt
        state.y = initialState.y + derivative.dy*dt
        state.vx = initialState.vx + derivative.dvx*dt
        state.vy = initialState.vy + derivative.dvy*dt
        ax, ay = self.acceleration(state, t+dt)
        return Derivative(state.vx, state.vy, ax, ay)

    def updateParticle(self, t, dt):
        """Runge-Kutta 4th order solution to update object's pos/vel."""
        a = self.initialDerivative(self.st1, t)
        b = self.nextDerivative(self.st1, a, t, dt*0.5)
        c = self.nextDerivative(self.st1, b, t, dt*0.5)
        d = self.nextDerivative(self.st1, c, t, dt)
        dxdt = 1.0/6.0 * (a.dx + 2.0*(b.dx + c.dx) + d.dx)
        dydt = 1.0/6.0 * (a.dy + 2.0*(b.dy + c.dy) + d.dy)
        dvxdt = 1.0/6.0 * (a.dvx + 2.0*(b.dvx + c.dvx) + d.dvx)
        dvydt = 1.0/6.0 * (a.dvy + 2.0*(b.dvy + c.dvy) + d.dvy)
        self.st1.x += dxdt*dt
        self.st1.y += dydt*dt
        self.st1.vx += dvxdt*dt
        self.st1.vy += dvydt*dt

        a = self.initialDerivative(self.st2, t)
        b = self.nextDerivative(self.st2, a, t, dt*0.5)
        c = self.nextDerivative(self.st2, b, t, dt*0.5)
        d = self.nextDerivative(self.st2, c, t, dt)
        dxdt = 1.0/6.0 * (a.dx + 2.0*(b.dx + c.dx) + d.dx)
        dydt = 1.0/6.0 * (a.dy + 2.0*(b.dy + c.dy) + d.dy)
        dvxdt = 1.0/6.0 * (a.dvx + 2.0*(b.dvx + c.dvx) + d.dvx)
        dvydt = 1.0/6.0 * (a.dvy + 2.0*(b.dvy + c.dvy) + d.dvy)
        self.st2.x += dxdt*dt
        self.st2.y += dydt*dt
        self.st2.vx += dvxdt*dt
        self.st2.vy += dvydt*dt

        a = self.initialDerivative(self.st3, t)
        b = self.nextDerivative(self.st3, a, t, dt*0.5)
        c = self.nextDerivative(self.st3, b, t, dt*0.5)
        d = self.nextDerivative(self.st3, c, t, dt)
        dxdt = 1.0/6.0 * (a.dx + 2.0*(b.dx + c.dx) + d.dx)
        dydt = 1.0/6.0 * (a.dy + 2.0*(b.dy + c.dy) + d.dy)
        dvxdt = 1.0/6.0 * (a.dvx + 2.0*(b.dvx + c.dvx) + d.dvx)
        dvydt = 1.0/6.0 * (a.dvy + 2.0*(b.dvy + c.dvy) + d.dvy)
        self.st3.x += dxdt*dt
        self.st3.y += dydt*dt
        self.st3.vx += dvxdt*dt
        self.st3.vy += dvydt*dt

        a = self.initialDerivative(self.st4, t)
        b = self.nextDerivative(self.st4, a, t, dt*0.5)
        c = self.nextDerivative(self.st4, b, t, dt*0.5)
        d = self.nextDerivative(self.st4, c, t, dt)
        dxdt = 1.0/6.0 * (a.dx + 2.0*(b.dx + c.dx) + d.dx)
        dydt = 1.0/6.0 * (a.dy + 2.0*(b.dy + c.dy) + d.dy)
        dvxdt = 1.0/6.0 * (a.dvx + 2.0*(b.dvx + c.dvx) + d.dvx)
        dvydt = 1.0/6.0 * (a.dvy + 2.0*(b.dvy + c.dvy) + d.dvy)
        self.st4.x += dxdt*dt
        self.st4.y += dydt*dt
        self.st4.vx += dvxdt*dt
        self.st4.vy += dvydt*dt

def engine(bike, terrain, dt):
    """Engine function"""
    status = True
    g = -9.8
    F = 2
    t = 0. #dummy variable
    dv = F*dt/bike.circle1.mass
    p = Particle(bike)
    p.updateParticle(t, dt)
    trigger1 = touch(p.st1, terrain)
    trigger2 = touch(p.st2, terrain)
    trigger3 = touch(p.st3, terrain)
    trigger4 = touch(p.st4, terrain)
    if trigger3 != -1 or trigger4 != -1:
        status = False
    if trigger1 != -1:
        alpha = terrain.alpha(touch(p.st1, terrain))
        p.st1 = revert(p.st1, alpha, dv)
    if trigger2 != -1:
        alpha = terrain.alpha(touch(p.st2, terrain))
        p.st2 = revert(p.st2, alpha, dv)
    bike.circle1 = p.st1
    bike.circle2 = p.st2
    bike.circle3 = p.st3
    bike.circle4 = p.st4
 

    return status, bike



def main():

    """ This is example for one spring """ 

    
    kappa = [12.0, 13.0, 14.0, 23.0, 24.0, 34.0]
    el0 = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0]
    m = [0.1, 0.1, 10., 2.]  
    r = [1., 1., 1., 0.2]
    xes = [0., 5., 0., 5.]
    yes = [0., 0., 5., 5.]

    terr = Terrain(10, 500)
    #terr.initialize('plain')
    terr.initialize('plain')

    b = Bike(kappa, el0, m, r, xes, yes)
    
    print touch(b.circle1, terr), terr.x(touch(b.circle1, terr))
    print touch(b.circle2, terr), terr.x(touch(b.circle2, terr))
    print touch(b.circle3, terr), terr.x(touch(b.circle3, terr))
    print touch(b.circle4, terr), terr.x(touch(b.circle4, terr))

    b.circle1.vx = 1.0
    b.circle1.vy = -2.0
    print b.circle1.vx, b.circle1.vy    
    b.circle1 = revert(b.circle1, 0.6, 2.0)
    print b.circle1.vx, b.circle1.vy
    
    p = Particle(b)



    t, dt = 0., 0.01

    x1 = []
    y1 = []
    x2 = []
    y2 = []
    x3 = []
    y3 = []
    x4 = []
    y4 = []
    tpoints = []
    
    for i in range(1):
       t += dt
       p .updateParticle(t, dt)
       print t, p.st1.x, p.st1.y, p.st2.x, p.st2.y,  p.st3.x, p.st3.y,  p.st4.x, p.st4.y
       x1.append(p.st1.x)
       y1.append(p.st1.y)
       x2.append(p.st2.x)
       y2.append(p.st2.y)
       x3.append(p.st3.x)
       y3.append(p.st3.y)
       x4.append(p.st4.x)
       y4.append(p.st4.y)
       tpoints.append(t)

    print
    print
    print

    kappa = [0.001, 0.001, 0.001, 0.001, 0.001, 0.001]
    el0 = [1.0, 1.414, 1.0, 1.0, 1.414, 1.0]
    m = [0.1, 0.1, 1, 1]  
    r = [0.5, 0.5, 0.5, 0.5]
    xes = [3., 4., 3., 4.]
    yes = [3., 3., 4., 4.]
    b = Bike(kappa, el0, m, r, xes, yes)
    terr = Terrain(100, 5000)
    #terr.initialize('plain')
    terr.initialize('slope',slope=0.1)
    safebike = True
    t= 0
    dt = 0.01
    
    x1=b.circle1.x
    y1=b.circle1.y

    x2=b.circle2.x
    y2=b.circle2.y


    x3=b.circle3.x
    y3=b.circle3.y

    x4=b.circle4.x
    y4=b.circle4.y

    R1=b.circle1.radius
    R2=b.circle2.radius

    R3=b.circle3.radius
    R4=b.circle4.radius
    
    pylab.ion()

    #pylab.axes([-10,10,-10,10])

    lf1, = plt.plot( [-3, 3] , [-3, 3], color='w')

    cir1 = pylab.Circle((x1,y1), radius=R1,  fc='y')
    pylab.gca().add_patch(cir1)

    lc1, = plt.plot( [x1, x1] , [y1-R1, y1+R1], color='g')

    cir2 = pylab.Circle((x2,y2), radius=R2, fc='y')
    pylab.gca().add_patch(cir2)


    lc2, = plt.plot( [x2, x2] , [y2-R2, y2+R2], color='g')

    cir3 = pylab.Circle((x3,y3), radius=R3, fc='b')
    pylab.gca().add_patch(cir3)


    cir4 = pylab.Circle((x4,y4), radius=R4, fc='b')
    pylab.gca().add_patch(cir4)


    # create 3x3 grid to plot the artists
    # add a line
    l1, = plt.plot( [x1,x2] , [y1,y2], color='r')
    l2, = plt.plot( [x1,x3] , [y1,y3], color='r')
    l3, = plt.plot( [x1,x4] , [y1,y4], color='r')
    l4, = plt.plot( [x2,x3] , [y2,y3], color='r')
    l5, = plt.plot( [x2,x4] , [y2,y4], color='r')
    l6, = plt.plot( [x3,x4] , [y3,y4], color='r')


    pylab.axis('scaled')

    # plot terrain here
    MY_ARRAY_X = []
    MY_ARRAY_Y = []
    # collect x and y from terrain object, directly to data structure(s) useful for plot function 
    for i in xrange(N_POINTS+1):
        x = terr.x(i)
        y = terr.y(x)
        MY_ARRAY_X.append(x)
        MY_ARRAY_Y.append(y)

    # ready to plot, green o shaped  dots
    # plot dots, o-s
    plt.plot(MY_ARRAY_X,MY_ARRAY_Y,'go')
    # double plot default ==  lines
    plt.plot(MY_ARRAY_X,MY_ARRAY_Y)

    pylab.draw()
    

    
    while safebike:
        x1=b.circle1.x
        y1=b.circle1.y

        x2=b.circle2.x
        y2=b.circle2.y


        x3=b.circle3.x
        y3=b.circle3.y

        x4=b.circle4.x
        y4=b.circle4.y
        cir1.center = (x1,y1)
        cir2.center = (x2,y2)
        cir3.center = (x3,y3)
        cir4.center = (x4,y4)
        xs12 = [x1, x2]
        ys12 = [y1, y2]
        l1.set_data(xs12,ys12)	

        xs13 = [x1, x3]
        ys13 = [y1, y3]
        l2.set_data(xs13,ys13)	

        xs14 = [x1, x4]
        ys14 = [y1, y4]
        l3.set_data(xs14,ys14)	

        xs23 = [x2, x3]
        ys23 = [y2, y3]
        l4.set_data(xs23,ys23)	

        xs24 = [x2, x4]
        ys24 = [y2, y4]
        l5.set_data(xs24,ys24)	

        xs34 = [x3, x4]
        ys34 = [y3, y4]
        l6.set_data(xs34,ys34)	

        xsc1 = [x1,x1]
        ysc1 = [y1-R1,y1+R1]
        lc1.set_data(xsc1,ysc1)	

        xsc2 = [x2,x2]
        ysc2 = [y2-R2,y2+R2]
        lc2.set_data(xsc2,ysc2)	
        pylab.draw()
        
#        print t, b.circle1.x, b.circle1.y, b.circle2.x, b.circle2.y, b.circle3.x, b.circle3.y, b.circle4.x, b.circle4.y
        
        safebike, b = engine(b, terr, dt)
        t = t + dt    

        
    pylab.ioff()
    pylab.show()
           
if __name__ == "__main__":
    main()
