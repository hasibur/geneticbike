"""This module contains the plotting function for the bike
"""

# our project:
from bike import *

# external library :
##from pylab import *
import numpy as np
import matplotlib
from matplotlib.patches import Circle, Polygon
from matplotlib.collections import PatchCollection
import matplotlib.pyplot as plt


#  init params for first  bike hardcoded here
kappa = [12.0, 13.0, 14.0, 23.0, 24.0, 34.0]
el0 = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0]
m = [0.1, 0.1, 10., 2.]  
# make wheels's r larger than load's r 
r = [1., 1., 0.5, 0.7]
# make it NOT line, but rectangle 
##xes = [1., 3., 5., 7.]
##yes = [0., 2., 4., 6.]
xes = [2., 6., 3., 6.]
yes = [2., 2., 4., 5.]

# create object of size  defined above
a_bike = Bike(kappa, el0, m, r, xes, yes)


# bike ready, I need its geometry
# feed from bike to my vars  x and y  (arrays)
# note: Bike object could have been an array!

# init  x to be an array (an np array, with 1 dimension)
# collect from Bike object

x =  np.array([a_bike.circle1.x, a_bike.circle2.x, a_bike.circle3.x, a_bike.circle4.x  ] )
#0.1 is scaling 
x   = 0.1*x
##print(x)
y =  np.array([a_bike.circle1.y, a_bike.circle2.y, a_bike.circle3.y, a_bike.circle4.y  ] )
##print(y)
y   = 0.1*y
radii   = np.array([a_bike.circle1.radius, a_bike.circle2.radius, a_bike.circle3.radius, a_bike.circle4.radius  ] )
##print(radii)
#0.1 is scaling 
radii   = 0.1*radii


# prepare the scene for plotting:

fig=plt.figure()
ax=fig.add_subplot(111)
patches = []

# start creating objects:
# random 4 circles. 
# later no random but take form real bike object
N = 4

""" no more random coordinates 
# coordinates are generated randomly, all 4 of them
x       = np.random.rand(N)
# x here  comes in form: [ 0.71334207  0.74379642  0.67074144  0.92558971]
##print(x)
y       = np.random.rand(N)
radii   = 0.1*np.random.rand(N)
"""

for x1,y1,r in zip(x, y, radii):
    circle = Circle((x1,y1), r)
    patches.append(circle)
#note: circle is eliptical!


# now connect the same 4 circles with lines
# new patches to be born
# random polygons
##for i in range(N):
    ##a = np.random.rand(N,2)
    ##print(a)
    ##polygon = Polygon(a, True)
    ##patches.append(polygon)
# just 1 polygon, connecting all 4 circles:
## need coordiantes in this form:

##[ [ 0.21085439  0.0664397 ]
##  [ 0.28782169  0.54746587]
##  [ 0.58583768  0.29965042]
##  [ 0.18813891  0.12494506]]
## np.array([[1, 2], [3, 4]])
## np.array([[1, 2], [3, 4], [5, 6], [7, 8] )

# loop to create polygons
for i in range(1):
    ##a =[]
    #a = np.zeros(N,2)
    # call np, just to get the form right 
    #a = np.zeros((N,2))
    # fake a's values, need to send 8 numbers  in correct form
    ##a =  np.array([[0.1, 0.2], [0.3, 0.4], [0.5, 0.6], [0.7, 0.8] ] )
    a =  np.array([[x[0], y[0]], [x[1], y[1]], [x[2], y[2]], [x[3], y[3]] ] )
    ##print(a)
    # take a values same as wheel and load  centres. for a coordinates
    polygon = Polygon(a, True)
    patches.append(polygon)


# patch collection makes final picture
p = PatchCollection(patches, cmap=matplotlib.cm.jet, alpha=0.4)

# add colours to all patches
# no colour yet 
# colour first 2 wheels blue, next 2 circles : red . 10 is red, 80 is blue
# colors = [10, 10, 80, 80]
# feed blue colur to all lines, 10 to colour array
#colors = [10, 10, 80, 80,10,10,10,10]
# 1 have 5 pathes: 2 whels, 2 loads, and 1  bike-frame polygon
colors = [10, 10, 80, 80,10]
p.set_array(np.array(colors))

# push all the patches of the figure to axes
ax.add_collection(p)

# final show on the screen
plt.show() 
    
