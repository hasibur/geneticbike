import pylab


import numpy as np
import matplotlib.pyplot as plt
import matplotlib
#from matplotlib.collections import PatchCollection
import matplotlib.path as mpath
import matplotlib.patches as mpatches
#import matplotlib.lines as mlines


x1=0
y1=0

x2=2.0/2.0
y2=0


x3=0.5/2.0
y3=1.0/2.0

x4=1.0/2.0
y4=1.0/2.0

R1=0.5/2.0
R2=0.5/2.0

R3=0.25/2.0
R4=0.25/2.0

pylab.ion()

#pylab.axes([-10,10,-10,10])

lf1, = plt.plot( [-3, 3] , [3, 3], color='w')

cir1 = pylab.Circle((x1,y1), radius=R1,  fc='y')
pylab.gca().add_patch(cir1)

lc1, = plt.plot( [x1, x1] , [y1-R1, y1+R1], color='g')

cir2 = pylab.Circle((x2,y2), radius=R2, fc='y')
pylab.gca().add_patch(cir2)


lc2, = plt.plot( [x2, x2] , [y2-R2, y2+R2], color='g')

cir3 = pylab.Circle((x3,y3), radius=R3, fc='b')
pylab.gca().add_patch(cir3)


cir4 = pylab.Circle((x4,y4), radius=R4, fc='b')
pylab.gca().add_patch(cir4)


# create 3x3 grid to plot the artists
# add a line
l1, = plt.plot( [x1,x2] , [y1,y2], color='r')
l2, = plt.plot( [x1,x3] , [y1,y3], color='r')
l3, = plt.plot( [x1,x4] , [y1,y4], color='r')
l4, = plt.plot( [x2,x3] , [y2,y3], color='r')
l5, = plt.plot( [x2,x4] , [y2,y4], color='r')
l6, = plt.plot( [x3,x4] , [y3,y4], color='r')


pylab.axis('scaled')


pylab.draw()

n=200
for i in range(n):
    n = float(n)
    cir1.center = (x1+i/n,y1)
    cir2.center = (x2+i/n,y2)
    cir3.center = (x3+i/n,y3)
    cir4.center = (x4+i/n,y4)

    xs12 = [x1+i/n, x2+i/n]
    ys12 = [y1, y2]
    l1.set_data(xs12,ys12)	

    xs13 = [x1+i/n, x3+i/n]
    ys13 = [y1, y3]
    l2.set_data(xs13,ys13)	

    xs14 = [x1+i/n, x4+i/n]
    ys14 = [y1, y4]
    l3.set_data(xs14,ys14)	

    xs23 = [x2+i/n, x3+i/n]
    ys23 = [y2, y3]
    l4.set_data(xs23,ys23)	

    xs24 = [x2+i/n, x4+i/n]
    ys24 = [y2, y4]
    l5.set_data(xs24,ys24)	

    xs34 = [x3+i/n, x4+i/n]
    ys34 = [y3, y4]
    l6.set_data(xs34,ys34)	

    xsc1 = [x1+i/n,x1+i/n]
    ysc1 = [y1-R1,y1+R1]
    lc1.set_data(xsc1,ysc1)	

    xsc2 = [x2+i/n,x2+i/n]
    ysc2 = [y2-R2,y2+R2]
    lc2.set_data(xsc2,ysc2)	

    pylab.draw()

pylab.ioff()
pylab.show()

#plt.show()
