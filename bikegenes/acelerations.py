import sys
from bike import *

def a_wheel_A(circle, alpha):
   """ aceleration on the circle wheel that is in contact with the floor, due to  an applied
   force F paralel to the surface.     alpha is the arctangent of the slope at x
   circle is the one representing the Wheel."""

   Fp=F/circle.mass
   c=math.cos(alpha)
   s=math.sin(alpha)
   
   ax=(Fp*c)-(g*s*c)
   ay=Fp*s+(g*(c**2)+1)

   return ax,ay

def a_spring_mj(circ1, circ2, k12, l12):

    """The function calculates the elastic aceleration between pair  m and j circles.
       Force acting on m ! """
  
    x1=circ1.x
    x2=circ2.x
    y1=circ1.x
    y2=circ2.y

    """carefull here , dividing by mass to obtain aceleration."""
    m1=circ1.mass 

    dis=distance(x1,y1,x2,y2)
    c=(1.0-(l12/dis))*k12/m1
    
    amj_x= -c*(x2-x1)  
    amj_y= -c*(y2-y1)
    
    return amj_x, amj_y

def a_bike(bike, m):
    """
    """
    fx, fy = 0, 0

    cm = bike.circles(m)
    for j in range(1, bike.NID):
        if j == m:
            continue   
        cj = bike.circles(j)
        k = bike.springs(m,j).k
        l = bike.springs(m,j).l0
        ax, ay =+ a_spring_mj(cm, cj, k, l)
    return ax, ay


def  a_wheel_B(circle, alpha)
  """elastic forces on m"""
  

