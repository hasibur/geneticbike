"""
This module deals with the Gnome i.e. Gene, Organism etc
"""


import random
import math
from copy import deepcopy
from copy import copy
import bike



# derive a gene which holds the radius property of Circle, and can
# mutate into another character which will be used as Radius Gene of the Circles

class FloatGene(object):
    
    def __init__(self,nameVal):
        self.name = "FloatGene"
        if len(str(nameVal)) > 0: self.name = nameVal
        self.mutProb = 1.0
        self.mutAmt = 1.0
        
        self.randMin = 0.1
        self.randMax = 1.0
        self.isMutated = False
        self.geneValue = self.randomValue()
        
    def getGeneName(self):
        return self.name
    
    def setGeneName(self,genename):
        self.name = genename
        
    def __repr__(self):
        return self
    
    def setMutationProbability(self,floatVal):
        self.mutProb = floatVal
    def getMutationProbability(self):
        return self.mutProb
    
    def setMutationAmount(self,floatVal):
        self.mutAmt = floatVal
    def getMutationAmount(self):
        return self.mutAmt
    
    def setRandomRange(self,minVal,maxVal):
        self.randMin = minVal
        self.randMax= maxVal
    def getRandomMin(self):
        return self.randMin
    def getRandomMax(self):
        return self.randMax
    def getRandomRange(self):
        randPair = (self.randMin,self.randMax)
        return randPair
    
    def mutateGene(self):
        rnd = random.random()
        if rnd < self.mutProb :
            mutAmnt = self.getMutationAmount()
            if mutAmnt > 1.0 : mutAmnt = 1.0
            rndVal = float(self.randomValue() * mutAmnt)
            self.setGeneValue(rndVal)
            self.isMutated = True
        else:
            self.isMutated = False
            
    def isMutated(self):
        return self.isMutated
    
    def randomValue(self):
        minVal = self.randMin
        maxVal = self.randMax
        
        val = float(10.0*(minVal + ((maxVal - minVal) * random.random())))
        val = float(val / 10.0)
        
#        if minVal < 0:
#            print('Min : ', minVal, ' Max : ', maxVal, ' Random : ', val)
            
        return val
        
    def setGeneValue(self,floatVal):
        self.geneValue = floatVal
        
    def getGeneValue(self):
        return self.geneValue
        
class CircleGene(FloatGene):
    
    def __init__(self,name):
        super(CircleGene,self).__init__(name)
        self.setGeneName(name)
        #default mutation probability
        self.setMutationProbability(0.1)
        #default amount of mutation
        self.setMutationAmount(0.2)
        
        #setting default min and max random range for mutation 
        self.setRandomRange(float(1), float(5))
        self.setGeneValue(self.randomValue())
        

# derive a gene which holds the spring const property of Spring, and can
# mutate into another character which will be used as Spring Const Gene of the Spring
class SpringConstGene(FloatGene):
    def __init__(self,name):
        super(SpringConstGene,self).__init__(name)
        self.setGeneName(name)
        #default mutation probability
        self.setMutationProbability(0.1)
        #default amount of mutation
        self.setMutationAmount(0.2)
        
        #setting default min and max random range for mutation 
        self.setRandomRange(float(1), float(5))
        self.setGeneValue(self.randomValue())
        
    def getGeneValue(self):
        return abs(self.geneValue)

    
# derive a gene which holds the length property of Spring, and can
# mutate into another character which will be used as Length Gene of the Spring
class SpringGene(FloatGene):
    def __init__(self,name):
        super(SpringGene,self).__init__(name)
        self.setGeneName(name)
        #default mutation probability
        self.setMutationProbability(1.0)
        #default amount of mutation
        self.setMutationAmount(4.0)
        
        #setting default min and max random range for mutation 
        self.setRandomRange(float(12), float(30))
        self.setGeneValue(self.randomValue())
        
    
# derive a gene which holds the mass property of Spring, and can
# mutate into another character which will be used as Mass Gene of the Circles
class MassGene(FloatGene):
    def __init__(self,name):
        super(MassGene,self).__init__(name)
        self.setGeneName(name)
        #default mutation probability
        self.setMutationProbability(3)
        #default amount of mutation
        self.setMutationAmount(5)
        
        #setting default min and max random range for mutation 
        self.setRandomRange(float(5), float(50))
        self.setGeneValue(self.randomValue())

    
class InitOrganism(object):
    
    #the base Genome which contains the other charactaristic Genes
    def __init__(self):
        self.baseGenome = []
        self.initiateGenomeBike()
        
    def getBaseGenome(self):
        return deepcopy(self.baseGenome)
#        return copy(self.baseGenome)
    
    def __repr__(self):
        return self

    def initiateGenomeCurve(self):
        # x^3 + 5x^2 + 3x -1
        for i in xrange(0,1):
            x = i
            y = (math.pow(i,3)) + (5 * math.pow(i,2)) + (3 * i) -1
            
            genex = FloatGene(x)
            genex.setMutationProbability(1.0)
            genex.setMutationAmount(1.0)
            genex.setRandomRange(-50, 50)
            genex.setGeneValue(genex.randomValue())
            self.baseGenome.append(genex)
            
            geney = FloatGene(y)
            geney.setMutationProbability(1.0)
            geney.setMutationAmount(1.0)
            geney.setRandomRange(-2, 10)
            geney.setGeneValue(geney.randomValue())
            self.baseGenome.append(geney)   

    def initiateGenomeBike(self):
        key = "ActWheelRadius"
        self.baseGenome.append(CircleGene(key))
        key = "ActWheelMass"
        self.baseGenome.append(MassGene(key))
        
        key = "2ndWheelRadius"
        self.baseGenome.append(CircleGene(key))
        key = "2ndWheelMass"
        self.baseGenome.append(MassGene(key))
        
        key = "Load1Radius"
        self.baseGenome.append(CircleGene(key))
        key = "Load1Mass"
        self.baseGenome.append(MassGene(key))
        
        key = "Load2Radius"
        self.baseGenome.append(CircleGene(key))
        key = "Load2Mass"
        self.baseGenome.append(MassGene(key))
        
        key = "Spring12Len"
        self.baseGenome.append(SpringGene(key))
        key = "Spring13Len"
        self.baseGenome.append(SpringGene(key))
        key = "Spring14Len"
        self.baseGenome.append(SpringGene(key))
        key = "Spring23Len"
        self.baseGenome.append(SpringGene(key))
        key = "Spring24Len"
        self.baseGenome.append(SpringGene(key))
        key = "Spring34Len"
        self.baseGenome.append(SpringGene(key))
        
        key = "Spring12k"
        self.baseGenome.append(SpringConstGene(key))
        key = "Spring13k"
        self.baseGenome.append(SpringConstGene(key))
        key = "Spring14k"
        self.baseGenome.append(SpringConstGene(key))
        key = "Spring23k"
        self.baseGenome.append(SpringConstGene(key))
        key = "Spring24k"
        self.baseGenome.append(SpringConstGene(key))
        key = "Spring34k"
        self.baseGenome.append(SpringConstGene(key))

        
    
class Organism(object):
    
    def __init__(self,orgID):
        self.organismId = orgID
        self.generation = 1
        self.crossoverRate = 0.5
        self.baseGenome = {}
        self.fitnessValue = 0.0
        
    def __repr__(self):
        return self.fitnessValue
    
    def getCurrentFitness(self):
        return self.fitnessValue
    
    def setCurrentFitness(self,val):
        self.fitnessValue = val
    
    def setOrganismID(self,intVal):
        self.organismId = intVal
        
    def getOrganismID(self):
        return self.organismId
    
    def setGeneration(self,value):
        self.generation = value
        
    def getGeneration(self):
        return self.generation
    
    def getCrossOverRate(self):
        return self.crossoverRate
    
    def setCrossOverRate(self,val):
        self.crossoverRate = val
        
    def setGenome(self, baseGenome):
        if(baseGenome == None):
            base = InitOrganism()
            self.baseGenome = base.getBaseGenome()
        else:
            self.baseGenome = baseGenome
            
    def fitness(self):
        pass
    
    def mutateOrganism(self):
        pass
    
    def numGenes(self):
        return len(self.baseGenome)
    
    def getGeneSequence(self):
        return self.baseGenomeList
    
    def getGenome(self):
        return deepcopy(self.baseGenome)
#        return copy(self.baseGenome)
    
    def getGene(self,id):
        return self.baseGenome[int(id)]
    
    def cullOrganisms(self):
        pass
    
    
class Population(object):
    
    def __init__(self):
        self.organisms = []
        # cull to this many children after each generation
        self.childCull = 0.5
        # number of children to create after each generation
        self.childCount = 20
        # number of initial population
        self.initPopulation = 20
        # max number of mutants per generation
        self.mutants = 0.25
        # survivors for reproduction depending on fitness condition
        self.survivors = []
        # best Fitness
        self.bestFitness = 0.0
        self.bestGenome = []
        
        
    def getBestOrganism(self):
        return self.bestGenome
    
    def getBestFitness(self):
        return self.bestFitness
        
    def setSurvivors(self,survOrgs):
        self.survivors = survOrgs
        
    def getSurvivors(self):
        return self.survivors
    
    def getSurvivor(self,index):
        return self.survivors[index]
    
    def addSurvivors(self,organism):
        self.survivors.append(organism)    
    
    def clearSurvivors(self):
        self.survivors = []
        
    def setChildCullmination(self,percentageVal):
        if percentageVal < 1.0:
            self.childCull = percentageVal
        
    def getChildCullmination(self):
        return self.childCull
    
    def setChildrenCount(self,intVal):
        self.childCount = intVal
        
    def getChildrenCount(self):
        return self.childCount
    
    def setInitPopulation(self,intVal):
        self.initPopulation = intVal
    
    def getInitPopulation(self):
        return self.initPopulation
    
    def setMaxMutants(self,percentageVal):
        self.mutants = percentageVal
        
    def getMaxMutants(self):
        return self.mutants
    
    def setOrganisms(self,Organisms):
        self.organisms = []
#        self.organisms = deepcopy(Organisms)
        self.organisms = copy(Organisms)
    
    def getOrganisms(self):
        return self.organisms
        
    def getOrganism(self,index):
        if index < len(self.organisms):
            return (self.organisms[index])
        else:
            return 0
    
    def numOrganism(self):
        return len(self.organisms)
    
    def __repr__(self):
        return self

    def best(self):
        minFit = 0.0
        
        for i in xrange(self.numOrganism()):
            organism = self.getOrganism(i)
            orgFitness = organism.fitness()
#            print('Organism : ', organism.getOrganismID(), ' Fitness : ', orgFitness, ' With Current Best : ', minFit)
            if minFit > orgFitness or minFit == 0.0:
                minFit = organism.fitness()
                self.bestGenome = deepcopy(organism.getGenome())

            if self.bestFitness >= minFit:
                self.bestFitness = minFit
        
        if (self.bestFitness == 0.0):
            self.bestFitness = minFit
            
#        print("Min : ", minFit, " Best : ", self.bestFitness)
        return minFit
    
    def reproduce(self):
        pass
                

#this class hold the base Genome with all the individual genes of an Organism
#initiating the organism by filling up with the individual genes and return the BaseGenome

    
# an organism i.e. bike that evolves
class NewOrganism(Organism):
    
    def __init__(self,OrgID):
        super(NewOrganism,self).__init__(OrgID)
        initOrg = InitOrganism()
        baseGenome = initOrg.getBaseGenome()
        self.setGenome(baseGenome)
        
    def mutateOrganism(self):
        self.matuteOrganismBike()
        
    def mutateOrganismCurve(self):
        for gene in self.baseGenome:
            gene.mutateGene()
            
    def matuteOrganismBike(self):
        for gene in self.baseGenome:
            gene.mutateGene()

    def fitness(self):
        """
        calculate fitness, as the sum of the product of
        the squares of the distance of each mass with mass itself 
        corresponding each object(wheel/load)
        """
        return self.fitnessBike()

        
    def fitnessCurve(self):
        idealx = -3.0
        idealy = 8.0
        
        genome = self.getGenome()
        # maxima y = 6x + 10
        newx = float(genome[0].getGeneValue())
        newy = float(genome[1].getGeneValue())
        diffx = idealx - newx
        diffy = idealy - newy
        diff = math.sqrt(math.pow(diffx, 2.0)+math.pow(diffy,2.0))
        
        self.fitnessValue = diff
        
        return self.fitnessValue
    
            
    def fitnessBike_(self):
            fitval = 0.0
            idealVal = 1.0
            genome = self.getGenome()
            
            for i in xrange(0,self.numGenes()-1,2):
                fitval += float(genome[i].getGeneValue()) * float(genome[i+1].getGeneValue())
                
            self.fitnessValue = fitval - idealVal
            
            return self.fitnessValue
                
                
class OrganismPopulation(Population):

    def __init__(self, initPop, cull, children, mutants):
        super(OrganismPopulation,self).__init__()
        
        self.setInitPopulation(initPop)
        if cull < 1.0: self.setChildCullmination(cull)
        self.setChildrenCount(children)
        self.setMaxMutants(mutants)
        
        organisms = []
        for i in xrange(int(initPop)):
            bike = NewOrganism(i)
            organisms.append(bike) 
        
        self.setOrganisms(organisms)
        
    def populate(self,organismList):
        organisms = []
        for organism in organismList:
            organisms.append(organism)
            
        self.setOrganisms(organisms)
        
    def reproduce(self):
        #sorting accroding to fitness
        generation = 0
        survivors = []
        oldOrganisms = deepcopy(self.getOrganisms())
        oldOrganisms.sort()
        cullmination = self.getChildCullmination()
        numOrg = self.numOrganism()
        
        cullNum = math.floor(cullmination * numOrg)
#        oldOrganisms = copy(self.getOrganisms())
        for organism in oldOrganisms:
            #sorted ascending order of fitness value
            
            if len(survivors) < cullNum:
                survivors.append(organism)
            else:
                break
            if generation == 0: generation = organism.getGeneration()
        
        if len(survivors) > 0:
            self.setSurvivors(survivors)
        else:
            self.setSurvivors(self.getOrganisms())
        #print("Number of Survivors : ",len(survivors))
        
        newOrgList = []
        childrenCount = self.getChildrenCount()
        cc = 0
        while len(newOrgList) < childrenCount:
            #choosing gamot 1 randomly
            gamot1 = None
            gamot2= None
            if len(self.survivors) > 2:
                numSurv = len(self.getSurvivors())
                val1 = random.randrange(0,numSurv-1,1)
                gamot1 = self.getSurvivor(val1)
                #choosing gamot 2 randomly
                val2 = random.randrange(0,numSurv-1,1)
                while val2 == val1:
                    val2 = random.randrange(0,numSurv-1,1)
                gamot2 = self.getSurvivor(val2)
            else:
                gamot1 = self.getSurvivor(0)
                gamot2 = self.getSurvivor(1)
                
            newGenome = self.mate(gamot1,gamot2)
            
            newOrganism = NewOrganism(str(cc+1))
            newOrganism.setGeneration(generation)
            newOrganism.setGenome(newGenome)
            
            rndMut = random.random()
            if (rndMut < self.getMaxMutants()):
                newOrganism.mutateOrganism()
                
            newOrgList.append(newOrganism)
        
        self.setOrganisms(newOrgList)
        
    def mate(self,parent1,parent2):
        gamot1 = parent1
        gamot2 = parent2
        
        crossRate = gamot1.getCrossOverRate()
        
        baseGenome1 = gamot1.getGenome()
        baseGenome1.sort()
        baseGenome2 = gamot2.getGenome()
        baseGenome2.sort()
        finalGenome = []
        geneCount = len(baseGenome1)
        
#        print("Init Genome Count : ", geneCount)
        
        for indx in xrange(1,geneCount+1):
            rnd = random.random()
            i = indx -1
            if (rnd < crossRate):
                geneVal1 = baseGenome1[i].getGeneValue()
                geneVal2 = baseGenome2[i].getGeneValue()
                finalGeneVal = float((geneVal1 + geneVal2)/2.0)
#                finalGeneVal = float(math.sqrt(math.pow(geneVal1,2)+math.pow(geneVal2,2)/2.0))
                newGene = baseGenome1[i]
                newGene.setGeneValue(finalGeneVal)
                finalGenome.append(newGene)
            else:
                rnd1 = random.random()
                if rnd1 <= 0.5:
                    newGene = baseGenome1[i]
                    finalGenome.append(newGene)
                else:
                    newGene = baseGenome2[i]
                    finalGenome.append(newGene)
        
#        print("New Genome Count : ", len(finalGenome))
                    
        return finalGenome
            
