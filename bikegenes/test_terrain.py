"""Testing for terrain module.
"""

from terrain import *

l = 20
N = 10

a_terrain = Terrain(l, N)

a_terrain.initialize('slope', slope=0.2)

print "-" *79
print "\nTesting Terrain\n"

#Testing definition and dx function
print "-" *79      
print "\tTerrain length = %f" % a_terrain.length
print "\tNumber of points = %d" % a_terrain.npoints
print "\tSpacing = %f\n" % a_terrain.dx()
print a_terrain.terrain
print

#Testing x function
print "-" *79 
print "\nThe x-values associated to the counters are: \n"
for i in xrange(N+1): print "%d -->\t%f" % (i, a_terrain.x(i))
print

#Testing counter function
print "-" *79 
print "\nThe counters associated to the x-values are: \n"
for i in xrange(N+1):
    x = i*float(l)/N
    print "%f -->\t%d" % (x, a_terrain.counter(x))
print

#Testing y function
print "-" *79 
print "\nThe y-values associated to the x-values are: \n"
for i in xrange(N+1):
    x = i*float(l)/N
    print "%f -->\t%f or %f" % (x, a_terrain.y(x), a_terrain.y(i))
print
    
#Testing alpha function
print "-" *79 
print "\nThe angles associated to the x-values are: \n"
for i in xrange(N+1):
    x = i*float(l)/N
    print "%f -->\t%f" % (x, a_terrain.alpha(x))
print

#Plotting coordinates to file
#output = open('test_terrain.dat','w')
#for i in xrange(N+1):
#    x = a_terrain.x(i)
#    y = a_terrain.y(x)
#    output.write("%f %f\n" % (x, y))
#output.close()

print a_terrain        
