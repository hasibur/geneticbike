#!/usr/bin/env python

'''
Created on 19-03-2013

@author: Hasibur
'''

#importing Genome and Bike Class
import Gnome as ga
import bike as bk
import math

'''
This function construct the bike list from a Organism Population

Input : Suitable Organism Population
Output : A list of Bike constructed from the organism list in a population 
            converting each organism to a bike with the genes value as the
            differents properties of the Bike 
'''
def constructBike(organism):
    # Collecting the Genes
#    key = "ActWheelRadius"
    geneWheel1Radius = organism.getGene(0)
    geneWheel1Radius.setRandomRange(0.5,10.0)

#    key = "ActWheelMass"
    geneWheel1Mass = organism.getGene(1)
    geneWheel1Mass.setRandomRange(1.0,10.0)
    
#    key = "2ndWheelRadius"
    geneWheel2Radius = organism.getGene(2)
    geneWheel2Radius.setRandomRange(0.5,5.0)
    
#    key = "2ndWheelMass"
    geneWheel2Mass = organism.getGene(3)
    geneWheel2Mass.setRandomRange(1.0,5.0)
    
#    key = "Load1Radius"
    geneLoad1Radius = organism.getGene(4)
    geneLoad1Radius.setRandomRange(0.5,5.0)

#    key = "Load1Mass"
    geneLoad1Mass = organism.getGene(5)
    geneLoad1Mass.setRandomRange(5.0,100.0)
    
#    key = "Load2Radius"
    geneLoad2Radius = organism.getGene(6)
    geneLoad1Radius.setRandomRange(0.5,5.0)
    
#    key = "Load2Mass"
    geneLoad2Mass = organism.getGene(7)
    geneLoad1Mass.setRandomRange(5.0,100.0)
    
#    key = "Spring12Len"
    geneSpring12Len = organism.getGene(8)
    geneSpring12Len.setRandomRange(1.0,20.0)

#    key = "Spring13Len"
    geneSpring13Len = organism.getGene(9)
    geneSpring13Len.setRandomRange(1.0,20.0)

#    key = "Spring14Len"
    geneSpring14Len = organism.getGene(10)
    geneSpring14Len.setRandomRange(1.0,20.0)
                                   
#    key = "Spring23Len"
    geneSpring23Len = organism.getGene(11)
    geneSpring23Len.setRandomRange(1.0,20.0)

#    key = "Spring24Len"
    geneSpring24Len = organism.getGene(12)
    geneSpring24Len.setRandomRange(1.0,20.0)

#    key = "Spring34Len"
    geneSpring34Len = organism.getGene(13)
    geneSpring34Len.setRandomRange(1.0,20.0)

    
#    key = "Spring12k"
    geneSpring12k = organism.getGene(14)
    geneSpring12k.setRandomRange(-0.01,-0.9)

#    key = "Spring13k"
    geneSpring13k = organism.getGene(15)
    geneSpring13k.setRandomRange(0.01,0.9)

#    key = "Spring14k"
    geneSpring14k = organism.getGene(16)
    geneSpring14k.setRandomRange(0.01,0.9)

#    key = "Spring23k"
    geneSpring23k = organism.getGene(17)
    geneSpring23k.setRandomRange(0.01,0.9)

#    key = "Spring24k"
    geneSpring24k = organism.getGene(18)
    geneSpring24k.setRandomRange(0.01,0.9)

#    key = "Spring34k"
    geneSpring34k = organism.getGene(19)
    geneSpring34k.setRandomRange(0.01, 0.9)
    
    # Building List Items for Spring Constants for the Bike
    spring_constants = []
    spring_constants.append(abs(geneSpring12k.getGeneValue()))
    spring_constants.append(abs(geneSpring13k.getGeneValue()))
    spring_constants.append(abs(geneSpring14k.getGeneValue()))
    spring_constants.append(abs(geneSpring23k.getGeneValue()))
    spring_constants.append(abs(geneSpring24k.getGeneValue()))
    spring_constants.append(abs(geneSpring34k.getGeneValue()))
    
    # Building List Items for Spring Lengths for the Bike 
    spring_lengths = []
    spring_lengths.append(geneSpring12Len.getGeneValue())
    spring_lengths.append(geneSpring13Len.getGeneValue())
    spring_lengths.append(geneSpring14Len.getGeneValue())
    spring_lengths.append(geneSpring23Len.getGeneValue())
    spring_lengths.append(geneSpring24Len.getGeneValue())
    spring_lengths.append(geneSpring34Len.getGeneValue())
    
    # Building List Items for all the Masses for the Bike
    masses = []
    masses.append(geneWheel1Mass.getGeneValue())
    masses.append(geneWheel2Mass.getGeneValue())
    masses.append(geneLoad1Mass.getGeneValue())
    masses.append(geneLoad2Mass.getGeneValue())
    
    # Building List Items for all the Radius for the Bike
    radii = []
    radii.append(geneWheel1Radius.getGeneValue())
    radii.append(geneWheel2Radius.getGeneValue())
    radii.append(geneLoad1Radius.getGeneValue())
    radii.append(geneLoad2Radius.getGeneValue())
    
    # Building List of X Coordinates
    xs = []
    xs.append(0)
    xs.append(0)
    xs.append(0)
    xs.append(0)
    
    # Building List of Y Coordinates
    ys = []
    ys.append(0)
    ys.append(0)
    ys.append(0)
    ys.append(0)
    
    # Building the bike object
    bike = bk.Bike(spring_constants, spring_lengths , masses, radii,xs,ys)
    # adding bike in the list
    
    return bike

def main():
    print "Testing Genetic Algorithm to find Minimum Fitnes in 1000 Generation"
    print
    
    # Populating Organism Population
    # Args : 1) Number of Initial Population
    #        2) Percentage Value of Rejecting Unfit Organism
    #        3) Number of Children for each Reproduction
    #        4) Percentage Value of Mutation Probability for each Reproduced Population
    bp = ga.OrganismPopulation(20,0.8,20,0.1)
    
    # iteration limit for genetic reproduction
    iterationLimit = 1000
    
    i = 1
    # list objects for Printing
    bestFitnessVal = 0.0
    bestBkOrg = None
    bestBike = None
    while True:
        print
        print("Generation : ", i)
        print

        # getting the Bike Organisms
        bikeOrgs = bp.getOrganisms()
        # Looping over all the Bike Organisms
        for bikeOrg in bikeOrgs:
            # Constructing Bike List for Current Generation
            bike = constructBike(bikeOrg)
            # Run Physics thing on bike
            # ---
            # ---
            
            # setting the Fitness Value
            
            curFitness = 0.0 #Will be returned by physics engine
            
            # setting current fitness value for organism
            bikeOrg.setCurrentFitness(curFitness)
            # checking if it is the best
            if bestFitnessVal <= curFitness:
                # setting the best bike parameters
                bestFitnessVal = curFitness
                bestBike = bike
                bestBkOrg = bikeOrg
                
            print('Bike ID: ', bike.getBikeID() , ' Fitness: ', curFitness, ' with Best : ', bestFitnessVal)
            
            # cutoff point if the bestFitnessVal cross 100 or whatever
            if (bestFitnessVal) > 100:
                print
                print('Found Fitest Bike with ID : ', bestBike.getBikeID(), ' in Generation ', i, ' with Fitness : ' ,bestFitnessVal)
        
                exit()
        
        # Proceed for next generation through reproduction
        else:
            bp.reproduce()
        
        # cutoff point if the iteration reaches the threshold
        if i < iterationLimit:
            i += 1
        else:
            print
            print("Iteration Limit Excced...")
            print('Fitest Bike Found ... with Bike ID : ', bestBike.getBikeID(), ' with Fitness : ', bestFitnessVal)
            
            exit()
        
        
if __name__ == '__main__':
    main()
    