"""This module contains the definition of the bike class and all subclasses.
"""

from sys import exit
from math import sqrt

def distance(x1, y1, x2, y2):
    """Returns distance between two 2D points
    """

    dist = sqrt( (x2 - x1)**2 + (y2 - y1)**2)
    return dist
    

class Bike(object):
    """Definition of the bike class.

    The bike is composed of 4 objects called "circles", which represent the
    wheels and loads and of 4 objects called "springs", that store the
    interactions. The first circle is the active wheel, the second is the
    passive wheel, while the other two are the loads.

    It requires as input 6 list containing all the spring constants, the spring
    rest lengths, the masses of the circles, the radii and the x and y
    positions.
    
    Contains functions:
    
    - chk_ID(ID) ---> Check ID
    - circles(ID) ---> Connect IDs to circles
    - springs(ID1, ID2) ---> Connects couples of IDs to springs
    - bdist(ID1, ID2) ---> Bike Distance
    """

    def __init__(self, spring_constants, spring_lengths , masses, radii,
                     xs, ys):
        #circle1 will always be the active wheel
        self.circle1 = Circle(1, masses[0], radii[0], xs[0], ys[0], 'ON')
        #circle2 will always be the passive wheel        
        self.circle2 = Circle(2, masses[1], radii[1], xs[1], ys[1], 'OFF')
        #circle3 and circle 4 will always be the loads
        self.circle3 = Circle(3, masses[2], radii[2], xs[2], ys[2], 'LOAD')        
        self.circle4 = Circle(4, masses[3], radii[3], xs[3], ys[3], 'LOAD')
        self.spring12 = Spring(1, 2, spring_constants[0], spring_lengths[0])
        self.spring13 = Spring(1, 3, spring_constants[1], spring_lengths[1])        
        self.spring14 = Spring(1, 4, spring_constants[2], spring_lengths[2])
        self.spring23 = Spring(2, 3, spring_constants[3], spring_lengths[3])
        self.spring24 = Spring(2, 4, spring_constants[4], spring_lengths[4])        
        self.spring34 = Spring(3, 4, spring_constants[5], spring_lengths[5])
        self.NID = 4       #number of IDs of the Bike class

    def getBikeID(self):
        return self.NID

    def chk_ID(self,ID):
        """Check if ID is a proper value.
        """

        if ID == 1 or ID == 2 or ID == 3 or ID == 4:
            chk = True
        else:
            chk = False
        return chk

    def circles(self, ID):
        """Returns the circle labelled with the ID.
        """

        if self.chk_ID(ID):
            label = "circle" + str(ID)
            return getattr(self,label)
        else:
            print "Error in circles function: WRONG IDS"
            exit(0)

    def springs(self, ID1, ID2):
        """Return the spring labelled with the two IDs in whatever order.
        """
        
        if self.chk_ID(ID1) and self.chk_ID(ID2) and ID1!=ID2:
            if ID1>ID2:
                i = ID2
                ID2 = ID1
                ID1 = i
            label = "spring" + str(ID1) + str(ID2)
            return getattr(self,label)
        else:
            print "Error in springs function: WRONG IDS"
            exit(0)
            
    def bdist(self, ID1, ID2):
        """Bike distance will return the distance between two circles inside
        the bike.
        """

        if self.chk_ID(ID1) and self.chk_ID(ID2):
            first = "circle" + str(ID1)
            second = "circle" + str(ID2)
            dist = distance(getattr(self, first).x,
                             getattr(self, first).y,
                             getattr(self, second).x,
                             getattr(self, second).y)
            return dist
        else:
            print "Error in bike distance: WRONG IDS"
            exit(0)



class Circle(object):
    """Definition of the circle class.

    The circle is the main component of the bike. Every circle has its own ID,
    its mass, its radius, the x and y position of the center and a particular
    flag to sort between different kinds of circle (active and passive wheels,
    loads).
    Another pair of parameters are the components of the velocity.

    Contains functions:

    - cdist(x,y) ---> Circle Distance
    """

    def __init__(self, ID, mass, radius, x, y, flag):
        self.ID = ID
        self.mass = mass
        if radius >= 0:
            self.radius = radius
        else:
            print "Error in initializing circle: WRONG RADIUS"
            exit(0)            
        self.x = x
        self.y = y
        self.vx = 0.0
        self.vy = 0.0
        if flag == 'ON' or flag == 'OFF' or flag == 'LOAD':
            self.flag = flag  
        else:
            print "Error in initializing circle: WRONG FLAG"
            exit(0)

    def cdist(self, x, y):
        """Circle distance will return the distance between the center of the 
        circle and another given point.
        """

        dist = distance(x, y, self.x, self.y)
        return dist
      

class Spring(object):
    """Definition of the spring class.

    Spring(ID1, ID2, k, l0) defines the spring constant and rest length of the 
    spring between circles ID1 and ID2
    """

    def __init__(self, ID1, ID2, k, l0):
        self.ID1 = ID1
        self.ID2 = ID2        
        if k > 0:
            self.k = k
        else:
            print "Error in initializing circle: WRONG SPRING CONSTANT"
            exit(0)
        self.l0 = l0

