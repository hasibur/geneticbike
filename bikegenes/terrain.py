"""This module contains the definition of the bike class and all subclasses.
"""

from sys import exit
from math import floor, tan
from random import random, uniform

class Terrain(object):
    """Definition of the terrain class.
    
    The terrain is mapped as a function which relates the y position and the
    angle with respect to the x axis as a function of the x position.
    
    It requires as input the total length of the terrain to be generated and
    the number of points.   
     
    The two functions can be adressed by using y(x) and alpha(x).
    
    There is also a built-in method to initialize the terrain.
    Just call   terrain_name.initialize(mode)
    The default terrain is an orrizontal line with the given length.   
    """

    def __init__(self, length, npoints):
        self.length = float(length)
        self.npoints = npoints
        self.terrain = []
        for i in xrange(self.npoints+1):
            self.terrain.append((0.0, 0.0))

    def __str__(self):
        terr_str = ""
        for i in xrange(self.npoints+1):
            terr_str += "%f %f\n" % (self.x(i), self.terrain[i][0])
        return terr_str

    def x(self, counter):
        """Returns the x value associated to the position in the array.
        """

        return counter*self.length/self.npoints

    def counter(self, x):
        """Returns the position in the array associated to the x value.
        """

        if x >= 0 and x<=self.length:
            count = int(floor(x*self.npoints/self.length))
        else:
            print "Error in evaluating terrain: REQUESTED X-VALUE OUT OF BONDS"
            exit(0)
        return count       

    def dx(self):
        """Returns the x projected distance betweeen two neighbouring points
        in the terrain.
        """

        return self.length/self.npoints

    def y(self, x):
        """Returns the y position associated to the x.

        If x is an intenger the function will refer to the index position,
        otherwise it will take the associated x-value
        """

        if isinstance(x, (int, long)):
            count = x
        else:
            count = self.counter(x)
        return self.terrain[count][0]

    def alpha(self, x):
        """Returns the angle with respect to the x axis associated to the x.

        If x is an intenger the function will refer to the index position,
        otherwise it will take the associated x-value
        """

        if isinstance(x, (int, long)):
            count = x
        else:
            count = self.counter(x)
        return self.terrain[count][1]

    def initialize(self, mode='plain', slope=0.2, slope_max=0.6, prob=0.05):
        """Built-in routine to initialize the terrain.
        
        Initialize the terrain y(x) and alpha(x) following the given mode.
        Depending on the given mode there may be needed other parameters.

        "PLAIN" MODE : returns a plain line

        "SLOPE" MODE : return a constant slope. You can specify the slope value
        by setting the "slope" parameter.

        "RANDOM-ZIG" MODE : the angle is kept fixed for a random length.
        Then a new angle is randomly chosen in a certain allowed range. You can
        specify the initial slope value with "slope", the maximum and minimum
        slope with "slope_max" and the changing probability with "prob".
        """

        if mode == 'PLAIN' or mode == 'plain':
            pass
        elif mode == 'SLOPE' or mode == 'slope':
            for i in xrange(self.npoints+1):
                x = i*float(self.length)/self.npoints
                self.terrain[i] = (x*tan(slope), slope)
        elif mode == 'RANDOM-ZIG' or mode == 'random-zig':
            slope_max = abs(slope_max)
            y = 0.0
            for i in xrange(self.npoints+1):
                x = i*float(self.length)/self.npoints
                y += tan(slope)*float(self.length)/self.npoints
                alpha = slope
                self.terrain[i] = (y, alpha)
                t = random()
                #prob*dx, not simple prob to avoid mesh dependencies
                if t < prob*self.dx():
                    slope = uniform(-slope_max, slope_max) 
        else:
            print "Error in initializing terrain: MODE NOT SUPPORTED"
            exit(0)

