"""Testing for the bike module
"""

from bike import *

kappa = [12.0, 13.0, 14.0, 23.0, 24.0, 34.0]
el0 = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0]
m = [0.1, 0.1, 10., 2.]  
r = [1., 1., 1., 0.2]
xes = [0., 5., 0., 5.]
yes = [0., 0., 5., 5.]

a_bike = Bike(kappa, el0, m, r, xes, yes)

print "-" *79
print "\nTesting Bike\n"
print "-" *79      
print "\nActive wheel\n"
print "\tmass = %f\n\tradius = %f\n\tposition = (%f, %f)" % (a_bike.circle1.mass,
                a_bike.circle1.radius, a_bike.circle1.x, a_bike.circle1.y)
print "-" *79      
print "\nPassive wheel\n"
print "\tmass = %f\n\tradius = %f\n\tposition = (%f, %f)" % (a_bike.circle2.mass,
                a_bike.circle2.radius, a_bike.circle2.x, a_bike.circle2.y)
print "-" *79      
print "\nFirst load\n"
print "\tmass = %f\n\tradius = %f\n\tposition = (%f, %f)" % (a_bike.circle3.mass,
                a_bike.circle3.radius, a_bike.circle3.x, a_bike.circle3.y)
print "-" *79      
print "\nSecond load\n"
print "\tmass = %f\n\tradius = %f\n\tposition = (%f, %f)" % (a_bike.circle4.mass,
                a_bike.circle4.radius, a_bike.circle4.x, a_bike.circle4.y)
print "-" *79   
print "\nSpring constant matrix\n"
print "\t%f\t%f\t%f\t%f" %  (0.0, a_bike.spring12.k, a_bike.spring13.k, 
                a_bike.spring14.k)
print "\t%f\t%f\t%f\t%f" %  (a_bike.spring12.k, 0.0, a_bike.spring23.k, 
                a_bike.spring24.k)
print "\t%f\t%f\t%f\t%f" %  (a_bike.spring13.k, a_bike.spring23.k, 0.0, 
                a_bike.spring34.k)
print "\t%f\t%f\t%f\t%f" %  (a_bike.spring14.k, a_bike.spring24.k,  
                a_bike.spring34.k, 0.0)
print "-" *79   
print "\nSpring length matrix\n"
print "\t%f\t%f\t%f\t%f" %  (0.0, a_bike.spring12.l0, a_bike.spring13.l0, 
                a_bike.spring14.l0)
print "\t%f\t%f\t%f\t%f" %  (a_bike.spring12.l0, 0.0, a_bike.spring23.l0, 
                a_bike.spring24.l0)
print "\t%f\t%f\t%f\t%f" %  (a_bike.spring13.l0, a_bike.spring23.l0, 0.0, 
                a_bike.spring34.l0)
print "\t%f\t%f\t%f\t%f" %  (a_bike.spring14.l0, a_bike.spring24.l0,  
                a_bike.spring34.l0, 0.0)
print "-" *79
print "\nDistance between (0,0) and (1,1) is %f" % distance(0,0,1,1)
print "-" *79
print "\nCheck (0) = %r" % a_bike.chk_ID(0)
print "Check (1) = %r" % a_bike.chk_ID(1)
print "Check (2) = %r" % a_bike.chk_ID(2)
print "Check (3) = %r" % a_bike.chk_ID(3)
print "Check (4) = %r" % a_bike.chk_ID(4)
print "Check (5) = %r" % a_bike.chk_ID(5)
print "-" *79
print "\nChecking circles --->  %f ?= %f" % (a_bike.circle4.mass, a_bike.circles(4).mass)
print "-" *79
print "\nChecking springs --->  %f ?= %f" % (a_bike.spring12.k, a_bike.springs(2,1).k)
print "-" *79
print "\nDistance between 1 and 2 is %f" % a_bike.bdist(1,2)
print "Distance between 1 and 3 is %f" % a_bike.bdist(1,3)
print "Distance between 1 and 4 is %f" % a_bike.bdist(1,4)
print "-" *79
print "\nDistance between 1 and (2.,2.) is %f" % a_bike.circle1.cdist(2.,2.)
print "Distance between 1 and (0.,2.) is %f" % a_bike.circle1.cdist(0.,2.)
print "Distance between 1 and (0.,1.) is %f" % a_bike.circle1.cdist(0.,1.)
print "-" *79
print "\nVelocity vector for 1 is (%f, %f)" % (a_bike.circle1.vx,a_bike.circle1.vy)
print "-" *79
print "\nNID = %d" % a_bike.NID

