"""This module contains the plotting functions for our objcects
"""

# our project:
from terrain import *

# external library :
from pylab import *

# define size of terrain and intialize  it:
# length
length = 100
# number of points
# more points for smoother surface
#N_POINTS = 20
# more points for smoother surface
N_POINTS = 100

# create object of size length N_POINTS
a_terrain = Terrain(length, N_POINTS)

# fill object
#a_terrain.initialize('plain', slope=0.2)
#a_terrain.initialize('random-zig', slope=0.0)
# tested slope
###a_terrain.initialize('slope', slope=0.2)
# tested zig-zag:
a_terrain.initialize('random-zig', slope=0.0)
# 

# terrain ready, I need its' X and Y arrays ... get them by looping through N_POINTS
#
#
MY_ARRAY_X = []
MY_ARRAY_Y = []
# collect x and y from terrain object, directly to data structure(s) useful for plot function 
for i in xrange(N_POINTS+1):
    x = a_terrain.x(i)
    y = a_terrain.y(x)
    MY_ARRAY_X.append(x)
    MY_ARRAY_Y.append(y)

# terrain is a 1 dim array of touples. wrong!
#  I need the array like so all x than all y:  [(1, 3, 5), (2, 2, 4)] or 2 arrays, separate for x and for Y

# debug printing
##print "\nMY_ARRAY_X\n"
##print(MY_ARRAY_X)

##print "\nMY_ARRAY_Y\n"
##print(MY_ARRAY_Y)

# ready to plot, green o shaped  dots
# plot dots, o-s
plot(MY_ARRAY_X,MY_ARRAY_Y,'go')
# double plot default ==  lines
plot(MY_ARRAY_X,MY_ARRAY_Y)

#other plots (opt):

# final show on the screen
show()

# 
    
